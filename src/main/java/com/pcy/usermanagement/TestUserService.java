/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.usermanagement;

/**
 *
 * @author Lenovo
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService.addUser("user2", "password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUsers());
        
        User user = UserService.getUser(3);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateUser(3, user);
        System.out.println(user);
        
        UserService.delUser(3);
        System.out.println(UserService.getUsers());
        
        System.out.println(UserService.login("admin", "password"));
       
        
    }
}
